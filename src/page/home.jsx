//类组件
import React from "react";
import ClassList from "../components/classComponents/class-list.jsx";
import ClassSearch from "../components/classComponents/class-search.jsx";
import ClassSelect from "../components/classComponents/class-select.jsx";
export default class Home extends React.Component {
  constructor() {
    super();
    //初始化变量
    this.state = {
      list: [],
      filterList: [],
      options: [],
    };
  }

  //组件挂载生命周期
  componentDidMount() {
    fetch("https://pokeapi.co/api/v2/pokemon")
      .then((res) => res.json())
      .then((json) => {
        json.results.forEach((result, index) => {
          result.id = index + 1;
        });
        this.setState(
          () => {
            return {
              list: json.results,
              filterList: json.results,
              options: [
                {
                  value: "jack",
                  label: "Jack",
                },
                {
                  value: "lucy",
                  label: "Lucy",
                },
                {
                  value: "Yiminghe",
                  label: "yiminghe",
                },
                {
                  value: "disabled",
                  label: "Disabled",
                  disabled: true,
                },
              ],
            };
          },
          () => {
            console.log("state", { ...this.state });
          }
        );
      });
  }

  //change事件
  changeInput = (e) => {
    console.log("value", e?.target?.value);
    const value = e?.target?.value;
    //过滤数组
    const result = this.state.list.filter((item) => {
      console.log("result", item.name.includes(value));
      return item.name.includes(value);
    });
    this.setState({ filterList: result });
  };
  changeSelect = (value) => {
    console.log("类组件 select改变", value);
  };
  //渲染
  render() {
    return (
      <div>
        <h1>类组件</h1>
        <ClassSearch changeInput={this.changeInput} />
        <ClassSelect
          options={this.state.options}
          changeSelect={this.changeSelect}
        />
        <ClassSelect
          options={this.state.options}
          changeSelect={this.changeSelect}
        />
        <ClassList list={this.state.filterList} />
        {/* <input type="search" onChange={this.changeInput}></input> */}
        {/* <ul>
          {this.state.filterList.map((item) => {
            return (
              <div key={item.id}>
                <li>{item.name}</li>
              </div>
            );
          })}
        </ul> */}
      </div>
    );
  }
}
