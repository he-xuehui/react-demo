import React from "react";
import FunList from "../components/funComponents/fun-list.jsx";
import FunSearch from "../components/funComponents/fun-search.jsx";
import FunSelect from "../components/funComponents/fun-select.jsx";
const FunHome = () => {
  const [list, setList] = React.useState([]);
  const [filterList, setFilterList] = React.useState([]);
  const [options, setOptions] = React.useState([
    {
      value: "jack",
      label: "Jack",
    },
    {
      value: "lucy",
      label: "Lucy",
    },
    {
      value: "Yiminghe",
      label: "yiminghe",
    },
    {
      value: "disabled",
      label: "Disabled",
      disabled: true,
    },
  ]);
  React.useEffect(() => {
    fetch("https://pokeapi.co/api/v2/pokemon")
      .then((res) => res.json())
      .then((json) => {
        json.results.forEach((result, index) => {
          result.id = index + 1;
        });
        setList(json.results);
        setFilterList(json.results);
        console.log("函数组件", list);
      });
  }, []);

  const changeInput = (e) => {
    const result = list.filter((item) => item.name.includes(e.target.value));
    setFilterList(result);
  };

  const changeSelect = (value) => {
    console.log("change value", value);
  };

  return (
    <div>
      <h1>函数组件</h1>
      <FunSearch changeInput={changeInput} />
      <FunSelect options={options} changeSelect={changeSelect} />
      <FunSelect options={options} changeSelect={changeSelect} />
      <FunList list={filterList} />
      {/* <input onChange={changeInput} type="search" />
      <ul>
        {filterList.map((item) => {
          return <li key={item.id}>{item.name}</li>;
        })}
      </ul> */}
    </div>
  );
};

export default FunHome;
