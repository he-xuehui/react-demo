import "./App.css";
import Demo1 from "./components/demo1.js";
import Home from "./page/home.jsx";
import FunHome from "./page/funHome.jsx";

function App() {
  return (
    <div className="App">
      <Demo1 />
      <br />
      <br />
      <Home />
      <br />
      <FunHome />
    </div>
  );
}

export default App;
