import React from "react";
import { Select, Space } from "antd";

export default class ClassSelect extends React.PureComponent {
  render() {
    const { defaultValue, options, changeSelect } = this.props;
    return (
      <Space wrap>
        <Select
          defaultValue={defaultValue ?? "Yiminghe"}
          options={options}
          onChange={changeSelect}
          style={{ width: 120 }}
        />
      </Space>
    );
  }
}
