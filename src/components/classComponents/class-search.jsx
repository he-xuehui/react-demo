import React from "react";
export default class ClassSearch extends React.Component {
  render() {
    const { changeInput } = this.props;
    return <input type="search" onChange={changeInput} />;
  }
}
