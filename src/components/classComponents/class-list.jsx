import React from "react";

export default class ClassList extends React.Component {
  constructor() {
    super();
    this.state = {
      childList: [],
    };
  }

  render() {
    const { list } = this.props;

    return (
      <ul>
        {list.map((item) => {
          return (
            <div key={item.id}>
              <li>{item.name}</li>
            </div>
          );
        })}
      </ul>
    );
  }
}
