import React from "react";
import { Select, Space } from "antd";

const FunSelect = ({ options, changeSelect, defaultValue }) => {
  return (
    <Space wrap>
      <Select
        defaultValue={defaultValue ?? "Yiminghe"}
        options={options}
        onChange={changeSelect}
        style={{ width: 120 }}
      />
    </Space>
  );
};

export default FunSelect;
