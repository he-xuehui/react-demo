// const FunSearch = ({ changeInput }) => {
//   //参数结构获取了change事件
//   return <input onChange={changeInput} type="search" />;
// };
// export default FunSearch;
//但是表达式定义的函数只能在定义后导出

//声明式函数可以在定义时直接导出
export default function FunSearch({ changeInput }) {
  return <input onChange={changeInput} type="search" />;
}
