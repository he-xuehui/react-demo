const FunList = ({ list }) => {
  //参数为父组件传入的所有props，做了结构
  return (
    <ul>
      {list.map((item) => {
        return <li key={item.id}>{item.name}</li>;
      })}
    </ul>
  );
};
export default FunList;
