import React from "react";

const MyInput = () => {
  const myInput = (e) => {
    console.log("inptu----e", e?.target?.value);
    console.log("state", this.state.name);
  };
  return (
    // <div>
    <>
      <input onInput={myInput} />
    </>
    // </div>
  );
};

// const Demo1 = () => {
//   const handleClick = () => {
//     console.log("点击按钮");
//   };
//   let str = "my str ";
//   return (
//     <div>
//       <span>{str}</span>
//       <MyInput />
//       <button onClick={handleClick}>点击按钮</button>
//     </div>
//   );
// };
class Demo1 extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      age: 0,
      str: "my str",
    };
  }
  //组件挂载
  componentDidMount() {
    this.getMyData();
  }
  getMyData = () => {
    this.setState({ name: "my name" });
  };
  handleClick = (msg) => {
    console.log("点击按钮", msg);
    this.setState({ name: "set my name" });
  };
  render() {
    return (
      <div>
        <div>{this.state.str}</div>
        <div>{this.state.name}</div>
        <MyInput />
        <button onClick={() => this.handleClick("msg")}>点击按钮</button>
      </div>
    );
  }
}
export default Demo1;
